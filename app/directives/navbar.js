(function() {
    'use strict';

    app.directive('navbar', ['$location', '$rootScope', 'AuthService', 'UserInfoService', '$http', '$state', function($location, $rootScope, AuthService, UserInfoService, $http, $state) {
        return {
            restrict: 'EA',
            templateUrl: 'app/navbar.tpl.html',
            link: function($scope) {
                $scope.location = $location.path();
                $rootScope.$on('$locationChangeStart', function(event, next) {
                    $scope.location = next.split('#')[1];

                });

                $scope.openMenu = function($mdOpenMenu, ev) {
                    $mdOpenMenu(ev);
                };

                $scope.logout = function() {
                    AuthService.logout();
                };

                $scope.openUserInfoModal = function() {
                    UserInfoService.showSettingsModal();
                };

                UserInfoService.init();
            }
        }
    }]);

})();