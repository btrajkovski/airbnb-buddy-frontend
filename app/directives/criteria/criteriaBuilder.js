(function () {
    'use strict';

    app.directive('criteriaBuilder', ['CriteriaService', '$mdToast', function (CriteriaService, $mdToast) {
        return {
            restrict: 'E',
            templateUrl: 'app/criteria/criteria-builder.tpl.html',
            scope: {
                ngModel: "="
            },
            link: function ($scope) {
                $scope.criteria = {};
                $scope.criterias = [];

                function init() {
                    $scope.criteria = {};
                    $scope.criterias = [];

                    $scope.criteria.location = '';
                    $scope.city = undefined;

                    CriteriaService.getCriteriaFields().then(function (response) {
                        $scope.criteriaFields = response.data;
                    });
                }
                init();

                $scope.addCriteria = function() {
                    $scope.criterias.push($scope.criteriaSelect);
                    $scope.criteria[$scope.criteriaSelect.name] = '';
                    $scope.criteriaSelect = undefined;
                };

                $scope.removeCriteria = function(criteriaName) {
                    var index = -1;
                    for (var i = 0; i < $scope.criterias.length; i++) {
                        if ($scope.criterias[i].name == criteriaName) {
                            index = i;
                        }
                    }
                    $scope.criterias.splice(index, 1);
                    delete $scope.criteria[criteriaName];
                };

                $scope.isSelected = function(name) {
                    return name === 'location' || $scope.criterias.filter(function(c) { return c.name === name}).length === 1;
                };

                function formatCriteria() {
                    var criteriaCopy = angular.copy($scope.criteria);

                    var Criteria = [];

                    _.map(Object.keys(criteriaCopy), function (o) {
                        if (criteriaCopy[o]) {
                            if (angular.isDate(criteriaCopy[o])) {
                                criteriaCopy[o] = moment(criteriaCopy[o]).format("YYYY-MM-DD");
                            }

                            Criteria.push({
                                name: o,
                                value: criteriaCopy[o]
                            });
                        }
                    });

                    return Criteria;
                }

                function showToast(message) {
                    var toast = $mdToast.simple()
                    .textContent(message)
                    .position('top right');

                    $mdToast.show(toast);
                }

                $scope.disableCriteriaButton = function() {
                    return _.filter(Object.keys($scope.criteria), function(o) {
                        return !!$scope.criteria[o];
                    }).length !== Object.keys($scope.criteria).length;
                };

                $scope.submit = function () {
                    var formatted = formatCriteria();
                    CriteriaService.newCriteria(formatted).then(function(response) {
                        showToast('Criteria created');
                        init();
                    }, function(response) {
                        showToast('Some error occured. Try again!');
                        console.log(response);
                    });
                }
            }
        }
    }]);

})();