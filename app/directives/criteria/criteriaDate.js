(function() {
    'use strict';

    app.directive('criteriaDate', [function() {
        return {
            restrict: 'E',
            templateUrl: 'app/criteria/criteria-date.tpl.html',
            scope: {
                ngModel: "=",
                name: "="
            },
            link: function($scope) {
                $scope.minDate = new Date();
            }
        }
    }]);

})();