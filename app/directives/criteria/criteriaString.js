(function() {
    'use strict';

    app.directive('criteriaString', [function() {
        return {
            restrict: 'E',
            templateUrl: 'app/criteria/criteria-string.tpl.html',
            scope: {
                ngModel: "=",
                name: "="
            },
            link: function($scope) {

            }
        }
    }]);

})();