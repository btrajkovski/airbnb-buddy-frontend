(function() {
    'use strict';

    app.directive('criteriaEnum', [function() {
        return {
            restrict: 'E',
            templateUrl: 'app/criteria/criteria-enum.tpl.html',
            scope: {
                ngModel: "=",
                name: "=",
                enums: "="
            },
            link: function($scope) {

            }
        }
    }]);

})();