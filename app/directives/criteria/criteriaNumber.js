(function() {
    'use strict';

    app.directive('criteriaNumber', [function() {
        return {
            restrict: 'E',
            templateUrl: 'app/criteria/criteria-number.tpl.html',
            scope: {
                ngModel: "=",
                name: "=",
                from: "=",
                to: "=",
                step: "="
            },
            link: function($scope) {

            }
        }
    }]);

})();