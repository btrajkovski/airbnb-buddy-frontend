(function () {
    'use strict';

    app.service('OfferService', OfferService);

    function OfferService($http, $q) {
        var service = {};

        service.getOffers = getOffers;

        return service;

        function getOffers(criteriaId, sort) {
            return $http.get('api/offers/' + criteriaId + '?sort=' + sort);
        }
    }
})();