(function () {
    'use strict';

    app.service('UserInfoService', UserInfoService);

    function UserInfoService($state, $http, $mdMedia, $mdDialog) {
        var service = {};

        service.showSettingsModal = showSettingsModal;
        service.init = init;
        service.getUserInfo = getUserInfo;
        service.saveUserInfo = saveUserInfo;

        return service;

        function getUserInfo() {
        	return $http.get('/api/user-info');
        }

        function saveUserInfo(userInfo) {
        	delete userInfo.lastNotified;
            return $http.post('/api/user-info', userInfo);
        }

        function init() {
        	getUserInfo().then(function(response) {
        		if (response.data === '') {
        			showSettingsModal();
        		}
        	});
        }

        function showSettingsModal() {
            var useFullScreen = $mdMedia('sm') || $mdMedia('xs');
            $mdDialog.show({
                controller: 'UserInfoModalController',
                templateUrl: 'app/user-info-modal.tpl.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                fullscreen: useFullScreen,
                onRemoving: function () {
                    init();
                }
            });
        }
    }
})();