(function () {
    'use strict';

    app.service('CriteriaService', CriteriaService);

    function CriteriaService($http, $q) {
        var service = {};

        service.getCriteriaFields = getCriteriaFields;
        service.newCriteria = newCriteria;
        service.getCriterias = getCriterias;
        service.deleteCriteria = deleteCriteria;

        return service;

        function getCriteriaFields() {
            return $http.get('api/fields');
        }

        function newCriteria(criteria) {
            return $http.post('api/criteria', criteria);
        }

        function getCriterias() {
            return $http.get('api/criteria');
        }

        function deleteCriteria(criteriaId) {
            return $http.delete('api/criteria/' + criteriaId);
        }
    }
})();