(function () {
    'use strict';

    app.controller('UserInfoModalController', function ($scope, $mdDialog, UserInfoService) {

        UserInfoService.getUserInfo().then(function(response) {
            $scope.userInfo = response.data || {};
        });

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.save = function() {
            $scope.error = null;
            if (!validateEmail($scope.userInfo.email)) {
                $scope.error = 'Invalid email';
                return;
            }
            if (!$scope.userInfo.notificationFrequency) {
                $scope.error = 'Notification frequency is required';
                return;
            }
            UserInfoService.saveUserInfo($scope.userInfo).then(function(response) {
                $mdDialog.hide();
            }, function() {
                $scope.cancel();
            });
        };

        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

    });
})();