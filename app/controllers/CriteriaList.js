(function () {
	'use strict';

	app.controller('CriteriaList', function ($scope, CriteriaService, $mdToast) {

		$scope.loaded = false;
		$scope.total = 0;
		var counter = 0;
		var criteriaFields, criterias;

		$scope.query = {
			limit: 10,
			page: 1
		};

		$scope.init = function() {
			counter = 0;
			CriteriaService.getCriteriaFields().then(function(response) {
				criteriaFields = response.data;
				loaded();
			});
			CriteriaService.getCriterias().then(function(response) {
				criterias = response.data;
				$scope.total = criterias.length;
				loaded();
			});
		};

		$scope.init();

		function loaded() {
			counter++;
			
			if (counter === 2) {

				var params = [];

				for (var i = 0; i < criterias.length; i++) {
					for (var j = 0; j < criterias[i].parameters.length; j++) {
						var param = criterias[i].parameters[j].name;
						if (params.indexOf(param) === -1) {
							params.push(param);
						}
					}
				}

				$scope.criteriaFields = params;
				var start = ($scope.query.page - 1) * $scope.query.limit;
				$scope.criterias = angular.copy(criterias).splice(start, $scope.query.limit);;

				$scope.loaded = true;
			}
		}

		$scope.getParam = function(criteria, paramName) {
			var filtered = criteria.parameters.filter(function(f) {
				return f.name === paramName;
			});
			return filtered.length > 0 ? filtered[0].value : 'N/A';
		};

		$scope.deleteCriteria = function(criteria) {
			CriteriaService.deleteCriteria(criteria.id).then(function (response) {
				if (response.status === 204) {
					showToast('Deleted successfully');
					$scope.init();
				} else {
					showToast('Some error occured. Try again.');
				}
			}, function() {
				showToast('Some error occured. Try again.');
			});
		};

		function showToast(message) {
			var toast = $mdToast.simple()
			.textContent(message)
			.position('top right');

			$mdToast.show(toast);
		}

	});
})();