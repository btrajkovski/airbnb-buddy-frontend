(function () {
    'use strict';

    app.controller('OfferController', function ($scope, $stateParams, OfferService) {

        $scope.offers = [];
        $scope.total = 0;

        $scope.pagination = {
            limit: 30,
            page: 1
        };

        $scope.sort = 'rating';

        $scope.init = function () {
            OfferService.getOffers($stateParams.id, $scope.sort).then(function(response) {
                $scope.offers = response.data;
                $scope.total = response.data.length;
            }, function(response) {
                console.log('error loading offers');
            });
            $scope.pagination.page = 1;
        };
        $scope.init();

        $scope.getStars = function(offer) {
            if (offer.star_rating === null) {
                return [];
            }
            var full = Math.floor(offer.star_rating);
            var half = offer.star_rating - full > 0;
            var stars = [];
            for (var i = 0; i < full; i++) {
                stars.push('full');
            }
            if (half) {
                stars.push('half');
            }
            return stars;
        };
    });
})();