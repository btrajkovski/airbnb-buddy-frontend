'use strict';

var app = angular.module('airbnb-buddy-frontend', [
    'ngMaterial',
    'ui.router',
    'ngResource',
    'ngCookies',
    'mdPickers',
    'md.data.table',
    'vsGoogleAutocomplete']);

app.config(function ($stateProvider, $urlRouterProvider, $httpProvider, $mdThemingProvider) {
    $stateProvider
        .state('home', {
            url: '/',
            controller: 'HomeController',
            templateUrl: 'app/home.tpl.html'
        })
		.state('offers', {
            url: '/offers/:id',
            controller: 'OfferController',
            templateUrl: 'app/offers.tpl.html'
        })
        .state('criteria-list', {
            url: '/criteria-list',
            controller: 'CriteriaList',
            templateUrl: 'app/criteria/criteria-list.tpl.html'
        });

    $urlRouterProvider.otherwise('/');

    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

    // $mdThemingProvider.theme('default')
    //     .primaryPalette('teal')
    //     .accentPalette('red');
});
